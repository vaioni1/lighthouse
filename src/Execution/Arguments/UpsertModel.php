<?php declare(strict_types=1);

namespace Nuwave\Lighthouse\Execution\Arguments;

use Nuwave\Lighthouse\Support\Contracts\ArgResolver;

class UpsertModel implements ArgResolver
{
    /** @var callable|\Nuwave\Lighthouse\Support\Contracts\ArgResolver */
    protected $previous;

    /** @param  callable|\Nuwave\Lighthouse\Support\Contracts\ArgResolver  $previous */
    public function __construct(callable $previous)
    {
        $this->previous = $previous;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  \Nuwave\Lighthouse\Execution\Arguments\ArgumentSet  $args
     */
    public function __invoke($model, $args)
    {
        // TODO consider Laravel native ->upsert(), available from 8.10
        $uniqueBy = method_exists($model, 'getUpsertUniqueBy')
        ? $model::getUpsertUniqueBy()
            : [$model->getKeyName()];

        $wheres = [];

        foreach ($uniqueBy as $key) {
            $wheres[$key] = $args->arguments[$key] ?? null;
        }

        $wheres = array_filter($wheres);

        if (!empty($wheres) || ($custom_lookup = method_exists($model, 'getUpsertUniqueQuery'))) {
            $existingModel = (isset($custom_lookup) && $custom_lookup)
                ? $model->getUpsertUniqueQuery($args->arguments)->first()
                : $model
                ->newQuery()
                ->where(function ($q) use ($wheres) {
                    foreach ($wheres as $key => $arg) {
                        $q->where($key, $arg->value);
                    }
                    return $q;
                })->first();

            if (null !== $existingModel) {
                $model = $existingModel;
            }
        }

        return ($this->previous)($model, $args);
    }
}
